##### 1.3.0
- added invalidateByArrayKey without sending subscription
- added the flag to prevent sending subscribe signal to invalidateAll

##### 1.2.0
- added invalidateAll to invalidate entire cache
- improved subscribing on invalidation

##### 1.1.0
- cache invalidation subscription

##### 1.0.2
- changed the documentation

##### 1.0.1
- changed the documentation

##### 1.0.0
- first version
