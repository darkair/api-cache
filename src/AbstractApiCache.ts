/**
 * Сервис кеширования данных API
 * Исключает повторные запросы, гарантирует получение данных
 */

import _cloneDeep from 'lodash-es/cloneDeep';

/**
 * NOTE: Использование Promise нужно, чтобы при частых запросах гарантированно возвращать результат,
 *       а не зависать в постоянной отмене старого и генерации нового запроса
 */
export interface CacheValue<T> {
    value: T;
    promise: Promise<T>;
    default: T;
}

export interface CacheArrayValue<T> {
    value: { [key: string]: T };
    promise: { [key: string]: Promise<T> };
    default: T;
}

export function makeCacheValue<T>(defValue: T): CacheValue<T> {
    return {
        value: undefined,
        promise: undefined,
        default: defValue,
    };
}

export function makeCacheArray<T>(defValue: T): CacheArrayValue<T> {
    return {
        value: undefined,
        promise: undefined,
        default: defValue,
    };
}

export interface ICache {}

export interface IApiCache<TCache extends ICache> {
    /**
     * Get the single value by the key
     */
    getValue<T>(name: keyof TCache, func: () => Promise<T>): Promise<T>;

    /**
     * Get the value from array by the unique key
     */
    getArrayValue<T>(name: keyof TCache, key: string, func: () => Promise<T>): Promise<T>;

    /**
     * Invalidate the cache by key
     */
    invalidate(name: keyof TCache): void;

    /**
     * Invalidate the cache by array key
     * Does not send subscription
     */
    invalidateByArrayKey(name: keyof Cache, key: string): void;

    /**
     * Invalidate entire cache
     */
    invalidateAll(): void;

    /**
     * Subscribe to cache invalidation
     */
    subscribe(name: keyof TCache, callback: () => void): () => void;
}

export abstract class AbstractApiCache<TCache extends ICache> implements IApiCache<TCache> {
    protected abstract cache: Record<keyof TCache, any>;
    private subscribers: Array<Array<() => void>> = [];

    /**
     * Get the single value by the key
     */
    async getValue<T>(name: keyof TCache, func: () => Promise<T>): Promise<T> {
        const n: string = name as string;

        if (this.cache[n].value !== undefined) {
            return _cloneDeep(this.cache[n].value);
        }
        if (!this.cache[n].promise) {
            this.cache[n].promise = new Promise(resolve => {
                func()
                    .then((data: T) => {
                        this.cache[n].value = data;
                        delete this.cache[n].promise;
                        resolve(_cloneDeep(data));
                    })
                    .catch(() => {
                        delete this.cache[n].promise;
                        resolve(_cloneDeep(this.cache[n].default));
                    });
            });
        }
        return this.cache[n].promise;
    }

    /**
     * Get the value from array by the unique key
     */
    async getArrayValue<T>(name: keyof TCache, key: string, func: () => Promise<T>): Promise<T> {
        const n: string = name as string;

        if (this.cache[n].value === undefined) {
            this.cache[n].value = {};
        }
        if (this.cache[n].value[key]) {
            return _cloneDeep(this.cache[n].value[key]);
        }

        if (this.cache[n].promise === undefined) {
            this.cache[n].promise = {};
        }
        if (!this.cache[n].promise[key]) {
            this.cache[n].promise[key] = new Promise(resolve => {
                func()
                    .then((data: T) => {
                        this.cache[n].value[key] = data;
                        delete this.cache[n].promise[key];
                        resolve(_cloneDeep(data));
                    })
                    .catch(() => {
                        // NOTE: В случае ошибки нужно вернуть корректное значение, но не запоминать его
                        //       В сервисе вызываем .catch(err => { throw CORRECT_VALUE })
                        delete this.cache[n].promise[key];
                        resolve(_cloneDeep(this.cache[n].default));
                    });
            });
        }
        return this.cache[n].promise[key];
    }

    /**
     * Invalidate the cache by key
     */
    invalidate(name: keyof TCache): void {
        const n: string = String(name);

        this.cache[n].value = undefined;
        this.cache[n].promise = undefined;

        // Send signal only at the end of microtask queue
        setTimeout(() => {
            (this.subscribers[n] ?? []).forEach((func: () => void) => func?.());
        }, 0);
    }

    /**
     * Invalidate the cache by array key
     * Does not send subscription
     */
    invalidateByArrayKey(name: keyof Cache, key: string): void {
        const n: string = String(name);

        if (this.cache[n].value !== undefined) {
            this.cache[n].value[key] = undefined;
        }
        if (this.cache[n].promise !== undefined) {
            this.cache[n].promise[key] = undefined;
        }
    }

    /**
     * Invalidate entire cache
     */
    invalidateAll(noSubscription: boolean = false): void {
        Object.keys(this.cache).forEach((n: string) => {
            this.cache[n].value = undefined;
            this.cache[n].promise = undefined;
        });

        if (noSubscription) {
            return;
        }
        // Send signal only at the end of microtask queue
        setTimeout(() => {
            Object.keys(this.cache).forEach((n: string) => {
                (this.subscribers[n] ?? []).forEach((func: () => void) => func?.());
            });
        }, 0);
    }

    /**
     * Subscribe to cache invalidation
     * @return Unsubscribe function
     */
    subscribe(name: keyof TCache, callback: () => void): () => void {
        const index: string = String(name);
        if (!this.subscribers[index]) {
            this.subscribers[index] = [];
        }
        this.subscribers[index].push(callback);
        return (): void => {
            const idx = this.subscribers[index].indexOf(callback);
            if (idx > -1) {
                this.subscribers[index].splice(idx, 1);
            }
        };
    }
}
